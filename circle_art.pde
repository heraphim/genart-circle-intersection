import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

String circlePos = "random"; // random horizontal vertical round
int maxCircles = 100; // maximum number of circles
int maxCircleSize = 250; // maximum circle size, ties with alpha of the lines
List<Circle> circles = new ArrayList<Circle>();
Iterator<Circle> circlesItt;
boolean drawCircles = false; // should we draw the actual circles
boolean circleGrowing = true; // are the circles growing?
boolean saveFrames = false;
boolean showFrameRate = false;
int restrictNoFrames = 0;

void setup() {
  fullScreen();
  ellipseMode(CENTER);
  background(0);
  circles.add(new Circle());
}

void draw() {
  noStroke();
  fill(0, 15);
  rect(0, 0, displayWidth, displayHeight); // partially cover the image each frame
  noFill();
  strokeWeight(1);
  int i = 0;
  for (Circle circle : circles) {
    if(drawCircles) {
      stroke(circle.colr, circle.life/2);
      ellipse(circle.c.x, circle.c.y, circle.r * 2, circle.r * 2);
    }

    for (int j = 0; j < i; j++) {
      Circle other = circles.get(j);
      if (other == circle) {
        continue;
      }
      if (circle.intersects(other)) {
        for (PVector point : circle.ints) {
          strokeWeight(1);
          stroke(circle.colr, circle.life / 10);
          line(circle.c.x, circle.c.y, point.x, point.y);
          stroke(other.colr, other.life / 10);
          line(other.c.x, other.c.y, point.x, point.y);
        }
      }
    }
    circle.update();
    i++;
  }
  circlesItt = circles.iterator();
  while (circlesItt.hasNext()) {
    Circle circle = circlesItt.next();
    if (circle.life < 1) {
      circlesItt.remove();
    }
  }
  int circlesNo = circles.size();
  if (circlesNo  < maxCircles) {
    circles.add(new Circle());
  }
  fill(0);
  noStroke();
  if(showFrameRate) {
    rect(0, 0, 60, 20);
    fill(255);
    text(floor(frameRate), 10, 10);
  }

  if(saveFrames) {
    saveFrame("screens/line-#########.png");
  }
  if(restrictNoFrames > 0 && restrictNoFrames == frameCount ) {
    noLoop();
  }
}

void keyPressed(){
  if(key=='s'||key=='S') {
    saveFrame("screens/line-#########.png");
  }
}
