
public class Circle {
  PVector c;
  float r = 1;
  float life;
  float speed;
  boolean alive = true;
  color colr;
  List<PVector> ints = new ArrayList<PVector>();
  Circle() {
    float x;
    float y;
    if("horizontal".equals(circlePos)) {
      x = random(displayWidth);
      y = displayHeight / 2;
    }
    else if("vertical".equals(circlePos)) {
      x = displayWidth / 2;
      y = random(displayHeight);
    }
    else if("round".equals(circlePos)){
      float angle = random(1) * TWO_PI;
      x = displayWidth / 2 + cos(angle) * 300;
      y = displayHeight / 2 + sin(angle) * 300;
    }
    else {
      y = random(displayHeight);
      x = random(displayWidth);
    }
    this.c = new PVector(x, y);
    this.speed = circleGrowing ? random(1) : 0;
    this.r = circleGrowing ? this.r : random(maxCircleSize);
    this.life = random(maxCircleSize);
    this.colr = random(1) > 0.9 ? color(random(155) + 100, random(155) + 100, random(155) + 100) :  color(255);
  }
  
  boolean intersects(Circle other) {
    float d = dist(this.c.x, this.c.y, other.c.x, other.c.y);
    if(d > (this.r + other.r) || d < abs(this.r - other.r)) {
      return false;
    }
    float a = (this.r * this.r - other.r * other.r + d * d) / (2 * d);
    PVector pa = PVector.add(this.c, PVector.div(PVector.mult(PVector.sub(other.c, this.c), a), d));
    if(d == (this.r + other.r)) {
      this.ints.add(pa);
      return true;
    }    
    float h = sqrt(this.r * this.r - a * a);
    this.ints.add(new PVector(pa.x + ((h*(other.c.y - this.c.y))/d), pa.y - ((h * (other.c.x - this.c.x)) / d)));
    this.ints.add(new PVector(pa.x - ((h*(other.c.y - this.c.y))/d), pa.y + ((h * (other.c.x - this.c.x)) / d)));
    return true;
  }
  
  void update() {
    this.ints.clear();
    this.r += this.speed;
    this.life -= this.speed;
    if(this.life < 2) {
      this.alive = false;
    }
    
  }
}
